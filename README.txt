This program is configured to run on utd dcXX servers out of the box, to run the application on different server change the config.dat file with required hostname.
=====================================================================================================================================================


COMPILING
==============
- The application requires Java 8 to run.

cd sctp
mkdir -p out/production/sctp
javac -d out/production/sctp src/edu/chetan/SctpNodeConfigProcessor.java
javac -d out/production/sctp -cp out/production/sctp src/edu/chetan/SctpBiDirectionalNode.java


RUNNING THE APPLICATION
========================
-To run the application, it requires config.dat file. If using different name change it in SctpNodeConfigProcessor.java and recompile it 

-The application takes nodeId as an program argument.
-If the nodeId specified as argument is not present in config file the application will exit with a warning about missing info.
-The application will output the info to files named "results". If this folder is not available the application will throw IOException and halt.

-Launching individual nodes manually

cd sctp/
mkdir results/ (required if not present)
java -cp out/production/sctp edu.chetan.SctpBiDirectionalNode {nodeId}


-You can also launch multiple nodes using remotelaunch.sh script.
-Make sure you have ssh keys configured on remote server prior to launch.
-Script will also grenate logs to verify with node{nodeId}.log naming structure for each node in logs folder.
-If logs are not required edit out the output redirection from launcher script. 

mkdir logs/ (only if logs are required)
bash remotelaunch.sh


OUTPUT FILES
=======================
-If launching with logs you can find them in logs/node{nodeId}.log file
-The output of N-Hop neighbours and eccentricity will be displayed on config-{nodeId}.out file, located in results/ folder.
