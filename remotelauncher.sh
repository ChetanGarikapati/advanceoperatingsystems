#!/bin/bash
username="cxg190009@";
domain=".utdallas.edu"
logs="logs/";
node="node";
logextension=".log";

cat config.dat | sed -e "s/#.*//" | sed -e "/^\s*$/d" |
{
 read i;
 echo "records to process: "$i;
 while [[ $n -lt  $i ]]
 do
  read line;
  node=$(echo $line | awk '{print $1}');
  hostname=$(echo $line | awk '{print $2}');
  query="ssh "${username}${hostname}${domain}" \"cd sctp; java -cp out/production/sctp edu.chetan.SctpBiDirectionalNode $node > "${logs}"node"${node}${logextension}" &\" ";
  n=$((n+1));
  echo "executing command: "$query;
  echo $query | bash -;
 done
}
