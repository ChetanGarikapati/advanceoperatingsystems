#!/bin/bash

username="cxg190009@";
domain=".utdallas.edu";

cat config.dat | sed -e "s/#.*//" | sed -e "/^\s*$/d" |
{ 
  read i;
  echo "records to process: "$i;
  while [[ $n -lt $i ]]
  do
    read line;
    node=$( echo $line | awk '{ print $1 }' );
    hostname=$( echo $line | awk '{ print $2 }' );
    query="ssh "${username}${hostname}${domain}" \" pkill java; rm -rf ~/sctp/logs/node*.*; rm -rf ~/sctp/results/config-*.* \" ";
    echo "executing command: "$query;
    echo ${query} | bash - ;
    n=$((n+1));
  done
}
