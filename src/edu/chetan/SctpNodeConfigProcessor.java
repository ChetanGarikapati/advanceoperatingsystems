package edu.chetan;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A helper class to parse the configuration file required by each SctpNode.
 *
 * @author Chetan Garikapati
 */
public class SctpNodeConfigProcessor {

    //HELPERS TO HOLD PARSED INFO OF CONFIG FILE
    private static int nodeCount = -1;
    private static int processedNeighbourCount = 0;
    private static int processedNodesCount = 0;

    //STANDARD MESSAGE HELPERS REQUIRED FOR PARSING
    private static final String SPACE = " ";
    private static final String COMMENT = "#";
    private static final String UTD_DOMAIN = ".utdallas.edu";

    //HOLDS INFO OF ALL PARSED NODES
    private static final HashMap<String, ArrayList<String>> nodeIdHostAndListenPortMap = new HashMap<>();
    private static final HashMap<String, Set<String>> nodeIdNeighboursMap = new HashMap<>();

    //DEFAULT FILE NAME OF THE CONFIGURATION FILE.
    private static final String CONFIG_FILE_NAME = "config.dat";

    private static final Logger LOGGER = Logger.getLogger("SctpNodeConfigProcessor");

    /**
     * Invoke this method to parse the config file.
     */
    public static void processConfigurationFile() {
        try {
            List<String> configDataLines = Files.readAllLines(Paths.get(CONFIG_FILE_NAME));
            configDataLines.stream().filter(configLine -> !configLine.isEmpty())
                    .filter(configLine -> Character.isDigit(configLine.charAt(0)))
                    .forEach(SctpNodeConfigProcessor::processConfigRecords);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Parses each line of config file to create an easily accessible map.
     *
     * @param configLine The line in config file to be parsed
     */
    private static void processConfigRecords(String configLine) {
        if (nodeCount == -1 && configLine.length() == 1) {
            nodeCount = Integer.parseInt(configLine);
            LOGGER.log(Level.INFO, "Number of records to be processed : " + nodeCount);
            return;
        }

        String[] nodeInfo = configLine.split(SPACE);
        if (processedNodesCount < nodeCount && nodeInfo.length >= 2
                && !(nodeInfo[0].equals(COMMENT) || nodeInfo[1].equals(COMMENT) || nodeInfo[2].equals(COMMENT))) {

            ArrayList<String> hostNameAndPort = new ArrayList<>();
            if (nodeInfo[1].startsWith("dc")) hostNameAndPort.add(nodeInfo[1].concat(UTD_DOMAIN));
            else hostNameAndPort.add(nodeInfo[1]);
            hostNameAndPort.add(nodeInfo[2]);
            nodeIdHostAndListenPortMap.put(nodeInfo[0], hostNameAndPort);
            processedNodesCount++;
        } else if (processedNeighbourCount < nodeCount) {
            HashSet<String> neighboursList = new HashSet<>();
            for (String neighbour : nodeInfo) {
                if (neighbour.equals(COMMENT)) break;
                neighboursList.add(neighbour);
            }
            nodeIdNeighboursMap.put(Integer.toString(processedNeighbourCount), neighboursList);
            processedNeighbourCount++;
        }

    }

    /**
     * Helper method to obtain hostname and port info of a specified node.
     *
     * @param nodeId the nodeId for which info is required.
     * @return Optional&lt;ArrayList&lt;String&gt;&gt; - containing host info at index 0 and port info at index 1.
     */
    public static Optional<ArrayList<String>> getHostAndPortInfo(String nodeId) {
        return Optional.ofNullable(nodeIdHostAndListenPortMap.get(nodeId));
    }

    /**
     * Helper method that returns direct neighbours list for specified node.
     *
     * @param nodeId The nodeId for which neighbours list is required
     * @return Optional&lt;Set&lt;String&gt;&gt; - containing set of neighbours.
     */
    public static Optional<Set<String>> getNodeNeighboursList(String nodeId) {
        return Optional.ofNullable(nodeIdNeighboursMap.get(nodeId));
    }

    /**
     * Helper method to obtain entire parsed info
     *
     * @return HashMap&lt;String, ArrayList&lt;String&gt;&gt; - The key is nodeId.
     */
    public static HashMap<String, ArrayList<String>> getNodeIdHostAndListenPortMap() {
        return nodeIdHostAndListenPortMap;
    }

    /**
     * Helper method to get the count of records/nodes parsed in config file
     *
     * @return int - count of records parsed
     */
    public static int getTotalNodeCount() {
        return nodeCount;
    }

    /**
     * Prints all the maps containing parsed info.
     */
    private static void printDebugInfo() {
        LOGGER.log(Level.FINE, "NodeId,HostName And PortInfo: " + nodeIdHostAndListenPortMap);
        LOGGER.log(Level.FINE, "NodeId And NeighbourMap: " + nodeIdNeighboursMap);
    }

    public static void main(String[] args) {
        SctpNodeConfigProcessor.processConfigurationFile();
        printDebugInfo();
    }
}
