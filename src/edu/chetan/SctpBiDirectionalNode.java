package edu.chetan;

import com.sun.nio.sctp.*;

import java.io.*;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.UnresolvedAddressException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A Node in a distributed system with bi-directional communication with its neighbours. <br><br>
 * The Node can identify the topology of the network and also syncs clock info in each computational round to
 * perform synchronized calculation.
 *
 * @author Chetan Garikapati
 */
public class SctpBiDirectionalNode {

    //NODE IDENTIFICATION VALUES
    private final String HOSTNAME;
    private final String NODEID;
    private final int LISTEN_PORT;

    //SYNCHRONIZED BLOCK EXECUTION HELPERS
    private int neighbourNodesCount = 0;
    private int clockUpdateCount = 0;
    private int synchronizedCalculationCount = 25;

    //NODE CLOCK VALUE STARTS AT 0
    private int clock = 0;
    private static final int DEFAULT_SHUTDOWN_TIME_IN_MINUTES = 2;
    private static boolean SHUTDOWN = false;
    private static final HashMap<Integer, HashSet<String>> clockValeNodeIdMap = new HashMap<>();

    //CONNECTION INFORMATION
    private Set<String> directNeighboursSet; // list of 1 hop neighbours
    private final Set<String> retryConnectionQueue = new ConcurrentSkipListSet<>(); //holds nodes info when connection fails and keeps retrying
    private final HashMap<Integer, String> associationAndNodeIdMap = new HashMap<>(); //associationId and NodeId
    private final HashMap<String, MessageInfo> nodeIdAndMessageInfoMap = new HashMap<>(); // nodeId and messageInfo containing remote port
    private final ConcurrentHashMap<String, Integer> nodeIdAndClockMap = new ConcurrentHashMap<>();   // nodeId and timer;
    private final HashMap<String, HashSet<String>> nodesAndNeighboursList = new HashMap<>(); // node and its neighbours list
    private final HashMap<String, SctpChannel> nodeAndChannelMap = new HashMap<>(); //nodeId and channel map for quick send

    //SERVER LISTENS ON "listenPort" FOR CONNECTIONS
    private SctpServerChannel sctpServerChannel;

    //STANDARD MESSAGES
    private static final String SPACE = " ";
    private static final String HELLO = "HELLO";
    private static final String PROBE_REQUEST = "PROBE_REQUEST";
    private static final String PROBE_RESPONSE = "PROBE_RESPONSE";
    private static final String CLOCK_INFO_MESSAGE = "CLOCK";

    //HELPERS
    private SelectionKey selectionKey;
    private SctpNotificationHandler sctpNotificationHandler;
    private ByteBuffer receiveBuffer;
    private HashSet<String> duplicateMessageCheckSet;
    private Selector connectionSelector;
    private Thread neighbourConnections;
    private String helloMessage;

    private static final Logger LOGGER = Logger.getLogger("SctpBiDirectionalNode");

    /**
     * Use this constructor if node info should be obtained from parsed config file and only nodeId is available.
     * The application terminates if info for specified nodeId is not available.
     *
     * @param nodeId The nodeId for the current node instance.
     */
    public SctpBiDirectionalNode(String nodeId) {
        Optional<ArrayList<String>> hostAndPortInfo = SctpNodeConfigProcessor.getHostAndPortInfo(nodeId);
        if (hostAndPortInfo.isPresent()) {
            this.NODEID = nodeId;
            this.HOSTNAME = hostAndPortInfo.get().get(0);
            this.LISTEN_PORT = Integer.parseInt(hostAndPortInfo.get().get(1));
        } else {
            this.NODEID = nodeId;
            HOSTNAME = null;
            LISTEN_PORT = 0;
            LOGGER.log(Level.WARNING, () -> "Config info for current node : " + nodeId + " not available in config file exiting");
            System.exit(0);
        }
    }

    /**
     * Use this constructor if all the node info is already known.
     * @param nodeId The nodeId for the current node instance.
     * @param hostName The hostname for the current node instance on which it is deployed.
     * @param listenPort The port for the current node instance to listen for incoming connections.
     */
    public SctpBiDirectionalNode(String nodeId, String hostName, int listenPort) {
        this.HOSTNAME = hostName;
        this.NODEID = nodeId;
        this.LISTEN_PORT = listenPort;

    }

    /**
     * This method gets its neighbours info from parsed config file.
     */
    private void getNeighboursInfo() {
        Optional<Set<String>> nodeNeighboursList = SctpNodeConfigProcessor.getNodeNeighboursList(NODEID);
        if (nodeNeighboursList.isPresent()) {
            directNeighboursSet = nodeNeighboursList.get();
            neighbourNodesCount = directNeighboursSet.size();
        }
    }

    /**
     * This method shuts down the application after predefined time has elapsed.
     * The time can be configured in DEFAULT_SHUTDOWN_TIME_IN_MINUTES variable.
     */
    private void enableTimeoutShutdown() {
        Runnable shutdownTimer = () -> {
            try {
                Thread.sleep(SctpBiDirectionalNode.DEFAULT_SHUTDOWN_TIME_IN_MINUTES * 60 * 1000);
                LOGGER.log(Level.INFO, "Timed out closing application of node : " + NODEID);
            } catch (InterruptedException e) {
                LOGGER.log(Level.INFO, "Execution complete shutting down application for node : " + NODEID);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                SHUTDOWN = true;
                synchronizedCalculationCount = 0;
            }
        };

        Thread timeoutShutDownThread = new Thread(shutdownTimer);
        timeoutShutDownThread.start();
    }

    /**
     * This method opens connections to its neighbours for communication and the connection stays open till the
     * application closes.
     *
     * @param neighboursSet The list of neighbour ids to which connection needs to be established.
     */
    private void connectToNeighbours(Set<String> neighboursSet) {
        try {
            InetSocketAddress remoteNeighbourSocketAddress = null;
            for (String neighbourNodeId : neighboursSet) {
                try {
                    Optional<ArrayList<String>> hostAndPortInfo = SctpNodeConfigProcessor.getHostAndPortInfo(neighbourNodeId);

                    //ONLY CONNECT TO NODES LESS THAN CURRENT NODE ID TO PREVENT MULTIPLE CONNECTIONS.
                    if (!neighbourNodeId.equals(NODEID) && !nodeIdAndMessageInfoMap.containsKey(neighbourNodeId) && hostAndPortInfo.isPresent()) {
                        remoteNeighbourSocketAddress = new InetSocketAddress(hostAndPortInfo.get().get(0),
                                Integer.parseInt(hostAndPortInfo.get().get(1)));
                        SctpChannel sctpChannel = SctpChannel.open(remoteNeighbourSocketAddress, 0, 0);
                        sctpChannel.configureBlocking(false);
                        sctpChannel.register(connectionSelector, sctpChannel.validOps());

                        System.out.println("connected to : " + neighbourNodeId);
                        MessageInfo outgoingMessageInfo = MessageInfo.createOutgoing(sctpChannel.association(), null, 0);
                        nodeIdAndMessageInfoMap.put(neighbourNodeId, outgoingMessageInfo);
                        associationAndNodeIdMap.put(sctpChannel.association().associationID(), neighbourNodeId);
                        nodeAndChannelMap.put(neighbourNodeId, sctpChannel);

                        //SENDING HELLO MESSAGES
                        System.out.println("Sending Hello Message to Node : " + neighbourNodeId + ",message: " + helloMessage);
                        sctpChannel.send(ByteBuffer.wrap(helloMessage.getBytes()), outgoingMessageInfo);

                        if (retryConnectionQueue.contains(neighbourNodeId)) {
                            System.out.println("Removing from retry queue");
                            retryConnectionQueue.remove(neighbourNodeId);
                        }
                    }
                } catch (UnknownHostException | UnresolvedAddressException e) {
                    e.printStackTrace();
                    System.out.println(neighbourNodeId + " " + (remoteNeighbourSocketAddress != null ? remoteNeighbourSocketAddress.getHostString() : null));
                } catch (ConnectException e) {
                    System.out.println("ConnectionException when connecting to node : " + neighbourNodeId);
                    LOGGER.log(Level.INFO, () -> "Retry connection for node : " + neighbourNodeId);
                    retryConnectionQueue.add(neighbourNodeId);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Computes the N-Hop neighbour hood of current node and prints out the eccentricity
     * i.e Number of hops to reach all the nodes.
     */
    private void computeNodeEccentricity() {
        try {
            int hopCount = 0;
            retryConnectionQueue.clear();
            HashMap<Integer, Set<String>> khopNeighbourMap = new HashMap<>();
            Set<String> allIdentifiedNodes = new HashSet<>();

            khopNeighbourMap.put(hopCount++, Collections.singleton(NODEID));
            khopNeighbourMap.put(hopCount, directNeighboursSet);
            allIdentifiedNodes.add(NODEID);
            allIdentifiedNodes.addAll(directNeighboursSet);

            //connect to all known nodes to get info
            connectToNeighbours(SctpNodeConfigProcessor.getNodeIdHostAndListenPortMap().keySet());

            //connect to all nodes requesting for PROBE_RESPONSE if HELLO was not received
            for (String neighbour : SctpNodeConfigProcessor.getNodeIdHostAndListenPortMap().keySet()) {
                if (!neighbour.equals(NODEID) && !nodesAndNeighboursList.containsKey(neighbour)) {
                    retryConnectionQueue.add(neighbour);
                    sendProbeRequest(neighbour);
                }
            }

            retryConnectionQueue.remove(NODEID);

            for (int i = 0; i < 10; i++) {
                if (!retryConnectionQueue.isEmpty()) {
                    connectToNeighbours(retryConnectionQueue);
                    Thread.sleep(1000);
                    System.out.println("slept");
                }
            }

            while (allIdentifiedNodes.size() < SctpNodeConfigProcessor.getTotalNodeCount()
                    && hopCount != SctpNodeConfigProcessor.getTotalNodeCount()) //2nd is safety condition for loop break
            {
                Set<String> nthHopNeighbours = new HashSet<>();
                khopNeighbourMap.get(hopCount).forEach(neighbour -> {
                    if (nodesAndNeighboursList.containsKey(neighbour)) {
                        nodesAndNeighboursList.get(neighbour).removeAll(allIdentifiedNodes);
                        nthHopNeighbours.addAll(nodesAndNeighboursList.get(neighbour));
                    } else {
                        System.out.println("Neighbour entry not available for node : " + neighbour);
                    }
                });
                allIdentifiedNodes.addAll(nthHopNeighbours);
                khopNeighbourMap.put(++hopCount, nthHopNeighbours);
            }

            //print log out to file and then create output file
            System.out.println();
            System.out.println("khopMap : " + khopNeighbourMap + " Eccentricity: " + hopCount);
            System.out.println("nodeCount: " + SctpNodeConfigProcessor.getTotalNodeCount());
            System.out.println("allIdentifiedNodes : " + allIdentifiedNodes);
            System.out.println("neighboursMap: " + nodesAndNeighboursList);
            System.out.println();

            File eccentricityOutputFile = new File("results/config-" + NODEID + ".out");
            eccentricityOutputFile.createNewFile();
            BufferedWriter eccentricityFileWriter = new BufferedWriter(new FileWriter(eccentricityOutputFile));

            eccentricityFileWriter.write("NODE ID: " + NODEID);
            eccentricityFileWriter.newLine();

            //Iterate over all khops of neighbours
            for (Map.Entry<Integer, Set<String>> entry : khopNeighbourMap.entrySet()) {
                Integer hop = entry.getKey();
                Set<String> neighboursList = entry.getValue();
                for (String neighour : neighboursList) {
                    eccentricityFileWriter.write(neighour + SPACE); //writes each node in this nth-hop
                }
                eccentricityFileWriter.write(" # hop count : " + hop);
                eccentricityFileWriter.newLine();
            }

            eccentricityFileWriter.write(hopCount + " # eccentricity");
            eccentricityFileWriter.newLine();
            eccentricityFileWriter.flush();
            eccentricityFileWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method sends a PROBE_REQUEST message to specified nodeId requesting its neighbours info. <br>
     * PROBE_REQUEST message format : PROBE_REQUEST &lt;REQUESTING_NODE_ID&gt; &lt;PROBE_FOR_NODE_ID&gt;
     *
     * @param requestForNodeId The nodeId to which a PROBE_REQUEST for its neighbours info should be sent.
     */
    private void sendProbeRequest(String requestForNodeId) {
        try {
            StringBuilder probeRequestMessageBuilder = new StringBuilder(PROBE_REQUEST).append(SPACE)
                    .append(NODEID).append(SPACE).append(requestForNodeId);

            //Try connecting to the requested node and then probe for neighbours list
            if (nodeAndChannelMap.containsKey(requestForNodeId)) {
                connectToNeighbours(Collections.singleton(requestForNodeId));
                if (!nodeAndChannelMap.containsKey(requestForNodeId)) return;
            }

            nodeAndChannelMap.get(requestForNodeId).send(ByteBuffer.wrap(probeRequestMessageBuilder.toString().getBytes()),
                    nodeIdAndMessageInfoMap.get(requestForNodeId));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method perform critical section of the synchronized block based on clock values of
     * current node and direct neighbours. <br>
     * After critical section is executed the method invokes eccentricity calculation method.
     */
    private void processSynchronizedCalculations() {

        try {
            while (nodeIdAndMessageInfoMap.size() < neighbourNodesCount) {
                connectToNeighbours(retryConnectionQueue);
                Thread.sleep(1000);
            }

            /*Thread eccentricityCalculationRoutine = new Thread(this::computeNodeEccentricity);
            eccentricityCalculationRoutine.start();*/

            LOGGER.log(Level.INFO, "Starting synchronized block for node : " + NODEID);
            sendClockUpdateToNeighbours(); //INITIAL CLOCK SYNC

            System.out.println("direct neighbours set for node " + NODEID + " : " + directNeighboursSet);

            while (synchronizedCalculationCount > 0) {
                for (String neighbourNodeId : directNeighboursSet) {
                    if (clockValeNodeIdMap.containsKey(clock) && clockValeNodeIdMap.get(clock).contains(neighbourNodeId)) {
                        clockUpdateCount++;
                    } else {
                        clockUpdateCount = 0;
                        Thread.sleep(1000);
                        break;
                    }
                }
                if (clockUpdateCount == neighbourNodesCount) {
                    clock++;
                    clockUpdateCount = 0;
                    System.out.println("clock updated to : " + clock + " map: " + nodeIdAndClockMap);
                    sendClockUpdateToNeighbours();
                    synchronizedCalculationCount--;
                }
            }

            computeNodeEccentricity();
            LOGGER.log(Level.INFO, "Synchronization block completed exiting node : " + NODEID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method sends a clock value to its direct neighbours, the message has following format <br>
     * CLOCK &lt;FROM_NODE_ID&gt; &lt;CLOCK_VALUE&gt
     */
    private void sendClockUpdateToNeighbours() {
        for (String neighbourId : directNeighboursSet) {
            try {
                SctpChannel sctpChannel = nodeAndChannelMap.get(neighbourId);
                String clockUpdateMessageBuilder = CLOCK_INFO_MESSAGE + SPACE +
                        NODEID + SPACE + clock;
                sctpChannel.send(ByteBuffer.wrap(clockUpdateMessageBuilder.getBytes()), nodeIdAndMessageInfoMap.get(neighbourId));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method will setup a sctpServerSocket and handles incoming connections and also invokes connection
     * requests to neighbours concurrently in a new thread.
     */
    private void configureConnections() {
        try {
            connectionSelector = Selector.open();
            neighbourConnections.start();//connect to neighbours
            sctpNotificationHandler = new SctpNotificationHandler();

            sctpServerChannel = SctpServerChannel.open();
            sctpServerChannel.bind(new InetSocketAddress(LISTEN_PORT));
            sctpServerChannel.configureBlocking(false);
            sctpServerChannel.register(connectionSelector, sctpServerChannel.validOps());

            receiveBuffer = ByteBuffer.allocate(2048);
            duplicateMessageCheckSet = new HashSet<>();

            LOGGER.log(Level.INFO, "Node : " + NODEID + " started listening for incoming connections");

            //WAIT TILL TIMEOUT TO CONNECT TO NEW CONNECTIONS
            while (!SHUTDOWN) {
                connectionSelector.select(DEFAULT_SHUTDOWN_TIME_IN_MINUTES * 60 * 100);
                Set<SelectionKey> selectionKeySet = connectionSelector.selectedKeys();
                Iterator<SelectionKey> selectionKeyIterator = selectionKeySet.iterator();

                while (selectionKeyIterator.hasNext()) {
                    selectionKey = selectionKeyIterator.next();

                    if (selectionKey.isAcceptable()) {
                        SctpChannel acceptedSctpChannel = sctpServerChannel.accept();
                        acceptedSctpChannel.configureBlocking(false);
                        acceptedSctpChannel.register(connectionSelector, acceptedSctpChannel.validOps());
                    }

                    if (selectionKey.isReadable()) {
                        handleReadMessage((SctpChannel) selectionKey.channel());
                        receiveBuffer.compact();
                        receiveBuffer.clear();
                    }

                    selectionKeyIterator.remove();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method prepares a HELLO message which includes current nodeId along with its direct neighbours. <br>
     * HELLO message format : HELLO &lt;FROM_NODE_ID&gt; &lt;HELLO_SRC_NODE_ID&gt; &lt;NEIGHBOURS LIST&gt;
     */
    private void prepareHelloMessage() {
        StringBuilder helloMessageBuilder = new StringBuilder(HELLO).append(SPACE).append(NODEID)
                .append(SPACE).append(NODEID);
        for (String neighbours : directNeighboursSet) {
            helloMessageBuilder.append(SPACE).append(neighbours);
        }
        helloMessage = helloMessageBuilder.toString();
    }

    /**
     * This method handles all the incoming messages and processes them accordingly
     *
     * @param sctpChannel The sctp channel through which a message was received.
     */
    private void handleReadMessage(SctpChannel sctpChannel) {
        try {
            sctpChannel.receive(receiveBuffer, System.out, sctpNotificationHandler);
            String receivedMessage = new String(receiveBuffer.array()).trim();
            if (!duplicateMessageCheckSet.contains(receivedMessage)) {

                duplicateMessageCheckSet.add(receivedMessage);
                System.out.println(receivedMessage);
                String[] splitInfo = receivedMessage.split(SPACE);

                switch (splitInfo[0]) {

                    //PROCESS HELLO MESSAGES : HELLO <FROM_NODE_ID> <HELLO_SRC_NODE_ID> <NEIGHBOURS LIST>
                    //PROCESS PROBE_RESPONSE MESSAGES WHICH CONTAINS NEIGHBOURS LIST : PROBE_RESPONSE <RESPONDING_NODE_ID> <NEIGHBOURS_FOR_NODE_ID> <NEIGHBOURS_LIST>
                    case HELLO:
                    case PROBE_RESPONSE: {

                        //CREATES AND ADDS MessageInfo FOR BI-DIRECTION COMMUNICATION ON SAME CHANNEL
                        if (!nodeIdAndMessageInfoMap.containsKey(splitInfo[1])) {
                            nodeIdAndMessageInfoMap.put(splitInfo[1], MessageInfo.createOutgoing(sctpChannel.association(), null, 0));
                            associationAndNodeIdMap.put(sctpChannel.association().associationID(), splitInfo[1]);
                            if (!nodeAndChannelMap.containsKey(splitInfo[1]))
                                nodeAndChannelMap.put(splitInfo[1], sctpChannel);
                        }

                        HashSet<String> neighbourSet = new HashSet<>();
                        Arrays.stream(splitInfo).skip(3).filter(neighbour -> !neighbour.isEmpty()).forEach(neighbourSet::add);
                        nodesAndNeighboursList.put(splitInfo[2], neighbourSet);

                        retryConnectionQueue.remove(splitInfo[1]);
                        LOGGER.log(Level.FINE, () -> "HELLO/PROBE_RESPONSE FROM NODE : " + splitInfo[1]);
                        break;
                    }

                    //SEND INFO ABOUT NEIGHBOURS IF IS INFO PRESENT : PROBE_REQUEST <REQUESTING_NODE_ID> <PROBE_FOR_NODE_ID>
                    case PROBE_REQUEST: {

                        if (nodesAndNeighboursList.containsKey(splitInfo[2]) || splitInfo[2].equals(NODEID)) {
                            sendProbeResponse(sctpChannel, splitInfo[2]);
                        }
                        break;
                    }

                    //UPDATE CLOCK INFO ABOUT NEIGHBOUR NODES : CLOCK <FROM_NODE_ID> <CLOCK_VALUE>
                    case CLOCK_INFO_MESSAGE: {

                        try {
                            int receivedClockValue = Integer.parseInt(splitInfo[2]);
                            nodeIdAndClockMap.put(splitInfo[1], receivedClockValue);
                            if (clockValeNodeIdMap.containsKey(receivedClockValue)) {
                                clockValeNodeIdMap.get(receivedClockValue).add(splitInfo[1]);
                            } else {
                                HashSet<String> neighboursInClockCycle = new HashSet<>();
                                neighboursInClockCycle.add(splitInfo[1]);
                                clockValeNodeIdMap.put(receivedClockValue, neighboursInClockCycle);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    }

                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * When a node sends a PROBE_REQUEST asking for nodes neighbours a response will be sent.
     *
     * @param sctpChannel             The sctpChannel through which a response should be sent.
     * @param nodeIdForNeighboursList The nodeId for which the neighbour nodes info is required.
     */
    private void sendProbeResponse(SctpChannel sctpChannel, String nodeIdForNeighboursList) {
        try {
            if (sctpChannel.isOpen() && selectionKey.isWritable()) {
                StringBuilder probeResponseMessageBuilder = new StringBuilder(PROBE_RESPONSE).append(SPACE).append(NODEID).append(SPACE)
                        .append(nodeIdForNeighboursList);

                if (nodeIdForNeighboursList.equals(NODEID)) {
                    directNeighboursSet.forEach(neighbour -> probeResponseMessageBuilder.append(SPACE).append(neighbour));
                } else {
                    nodesAndNeighboursList.get(nodeIdForNeighboursList).forEach(neighbour -> probeResponseMessageBuilder.append(SPACE).append(neighbour));
                }

                ByteBuffer sendBuffer = ByteBuffer.wrap(probeResponseMessageBuilder.toString().getBytes());
                sctpChannel.send(sendBuffer, MessageInfo.createOutgoing(sctpChannel.association(), null, 0));

                LOGGER.log(Level.FINE, probeResponseMessageBuilder::toString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            System.out.println("Invalid Argument Count, requires 1 argument - NodeId");
            System.exit(0);
        }

        SctpNodeConfigProcessor.processConfigurationFile();
        SctpBiDirectionalNode sctpBiDirectionalNode = new SctpBiDirectionalNode(args[0]);

        try {
            sctpBiDirectionalNode.getNeighboursInfo();
            sctpBiDirectionalNode.prepareHelloMessage();
            sctpBiDirectionalNode.enableTimeoutShutdown();
            sctpBiDirectionalNode.neighbourConnections = new Thread(() -> {
                sctpBiDirectionalNode.connectToNeighbours(sctpBiDirectionalNode.directNeighboursSet);
                sctpBiDirectionalNode.processSynchronizedCalculations();
            });
            sctpBiDirectionalNode.configureConnections();
            sctpBiDirectionalNode.printDebugInfo();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sctpBiDirectionalNode.connectionSelector.close();
            sctpBiDirectionalNode.sctpServerChannel.close();
            System.exit(0);
        }
    }

    /**
     * Prints all the hashmaps and clock info for debugging purpose.
     */
    private void printDebugInfo() {
        LOGGER.log(Level.INFO, toString());
        System.out.println("clockValeNodeIdMap: " + clockValeNodeIdMap);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SctpBiDirectionalNode{");
        sb.append("NODEID='").append(NODEID).append('\'');
        sb.append(", LISTEN_PORT=").append(LISTEN_PORT);
        sb.append(", neighbourNodesCount=").append(neighbourNodesCount);
        sb.append(", clockUpdateCount=").append(clockUpdateCount);
        sb.append(", synchronizedCalculationCount=").append(synchronizedCalculationCount);
        sb.append(", clock=").append(clock);
        sb.append(", directNeighboursSet=").append(directNeighboursSet);
        sb.append(", retryConnectionQueue=").append(retryConnectionQueue);
        sb.append(", nodeIdAndClockMap=").append(nodeIdAndClockMap);
        sb.append(", nodesAndNeighboursList=").append(nodesAndNeighboursList);
        sb.append(", duplicateMessageCheckSet=").append(duplicateMessageCheckSet);
        sb.append('}');
        return sb.toString();
    }

    /**
     * The notification handler class will be invoked when a message is received, when the shutdown notification
     * is received the application will stop checking if the channel is readable for incoming messages.
     */
    class SctpNotificationHandler extends AbstractNotificationHandler<PrintStream> {
        @Override
        public HandlerResult handleNotification(ShutdownNotification notification, PrintStream attachment) {
            attachment.printf("Removing association : %d of node : %s due to shutdown \n",
                    notification.association().associationID(), associationAndNodeIdMap.get(notification.association().associationID()));
            selectionKey.cancel();
            return super.handleNotification(notification, attachment);
        }
    }

}
