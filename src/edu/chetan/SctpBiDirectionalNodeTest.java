package edu.chetan;

import com.sun.nio.sctp.MessageInfo;
import com.sun.nio.sctp.SctpChannel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

public class SctpBiDirectionalNodeTest {

    private static final int PORT = 12234;

    private static final int TEST_NODE_ID = 3071;
    private static final String[] TEST_NEIGHBOURS_LIST = {"311", "313", "331", "32"};

    private SctpChannel sctpClientChannel;
    private InetSocketAddress inetSocketAddress;
    private MessageInfo testMessageInfo;

    private static final String SPACE = " ";
    private static final String HELLO = "HELLO";
    private static final String PROBE_REQUEST = "PROBE_REQUEST";
    private static final String PROBE_RESPONSE = "PROBE_RESPONSE";
    private static final String CLOCK_INFO_MESSAGE = "CLOCK";

    @DisplayName("Hello Message Test")
    @Test
    private void testHelloMessages() {
        try {
            inetSocketAddress = new InetSocketAddress("localhost", PORT);
            sctpClientChannel = SctpChannel.open(inetSocketAddress, 5, 5);

            StringBuilder helloMessageBuilder = new StringBuilder(HELLO).append(SPACE).append(TEST_NODE_ID)
                    .append(SPACE).append(TEST_NODE_ID);

            for (String neighbours : TEST_NEIGHBOURS_LIST) {
                helloMessageBuilder.append(SPACE).append(neighbours);
            }

            System.out.println(helloMessageBuilder);
            testMessageInfo = MessageInfo.createOutgoing(sctpClientChannel.association(),null,0);
            sctpClientChannel.send(ByteBuffer.wrap(helloMessageBuilder.toString().getBytes()),testMessageInfo);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    //PROBE_RESPONSE <RESPONDING_NODE_ID> <NEIGHBOURS_FOR_NODE_ID> <NEIGHBOURS_LIST>
    private void testProbeResponseMessages(){
        try {
            StringBuilder probeResponseBuilder = new StringBuilder(PROBE_RESPONSE).append(SPACE).append(TEST_NODE_ID)
                    .append(SPACE).append(TEST_NODE_ID);
            for (String neighbours : TEST_NEIGHBOURS_LIST) {
                probeResponseBuilder.append(SPACE).append(neighbours);
            }
            System.out.println(probeResponseBuilder);
            Thread.sleep(2000);
            sctpClientChannel.send(ByteBuffer.wrap(probeResponseBuilder.toString().getBytes()),testMessageInfo);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        SctpBiDirectionalNodeTest sctpBiDirectionalNodeTest = new SctpBiDirectionalNodeTest();
        sctpBiDirectionalNodeTest.testHelloMessages();
        sctpBiDirectionalNodeTest.testProbeResponseMessages();
    }


}
